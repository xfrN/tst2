// BTN top
const scrollToTopBtn = document.querySelector(".btn-top")
const rootElement = document.documentElement;
function scrollToTop() {
    rootElement.scrollTo({
        top: 0,
        behavior: "smooth"
    })
}
scrollToTopBtn.addEventListener("click", scrollToTop);





// VARS
const form = document.querySelector(".form");
const submit = document.querySelector("[type=submit]");
const success = document.querySelector(".form__msg--success");


const name = document.querySelector("[name=name]");
const email = document.querySelector("[name=email]");
const tel = document.querySelector('[name="phone"]');
const company = document.querySelector('[name="country"]');
const terms = document.querySelector('[name="terms"]');


const form_source = document.querySelector('[name="user_source"]');
const form_medium = document.querySelector('[name="user_medium"]');
const form_campaign = document.querySelector('[name="user_campaign"]');
const form_content = document.querySelector('[name="user_content"]');
const form_term = document.querySelector('[name="user_term"]');
const form_ref = document.querySelector('[name="user_ref"]');


let disable = false;
let send = false;

submit.disabled = disable;
let el = form.elements;
let place;


let today = new Date();
let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
let dateTime = date + ' ' + time;

let ref = document.referrer;
let target;
let cookie = false;

const queryString = window.location.search;
//console.log(queryString);
//console.log(source.value + '\n' + medium.value + '\n' + campaign.value);
const urlParams = new URLSearchParams(queryString);


// UTM
function get_utm(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    let results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

let utm = '',
    utm_trimmed = '',
    utm_source = '' || 'nothing found',
    utm_medium = '' || 'nothing found',
    utm_content = '' || 'nothing found',
    utm_campaign = '' || 'nothing found',
    utm_term = '' || 'nothing found';

(function () {
    utm_source = get_utm("utm_source");
    utm_medium = get_utm("utm_medium");
    utm_content = get_utm("utm_content");
    utm_campaign = get_utm("utm_campaign");
    utm_term = get_utm("utm_term");

    // window.localStorage.setItem('utm_medium', utm_medium);
    // window.localStorage.setItem('utm_source', utm_source);
    // window.localStorage.setItem('utm_campaign', utm_campaign);
    // window.localStorage.setItem('utm_term', utm_term);


    if (utm_source) {
        utm += '&utm_source=' + utm_source;
    }
    if (utm_medium) {
        utm += '&utm_medium=' + utm_medium;
    }
    if (utm_content) {
        utm += '&utm_content=' + utm_content;
    }
    if (utm_campaign) {
        utm += '&utm_campaign=' + utm_campaign;
    }
    if (utm_term) {
        utm += '&utm_term=' + utm_term;
    }

    if (utm.length > 0) {
        utm = utm.substring(1);
        utm_trimmed = utm;
        utm = '?' + utm;
    }

})();


// FORM SEND
function check_formSend(send) {
    if (send) {
        window.localStorage.setItem('send', true);
    }
    return send;
}

$(function () {
    $('.form').parsley().on('field:validated', function () {
        let ok = $('.parsley-error').length === 0;
        $('.bs-callout-info').toggleClass('hidden', !ok);
        $('.bs-callout-warning').toggleClass('hidden', ok);

    }).on('form:submit', function () {
        send = true;

        if (send) {
            success.classList.remove('d-none');
            check_formSend(send);
            //window.location.replace("https://target.com");
            submit.setAttribute("disabled", "disabled");
        }

        console.log(email.value);
        console.log(name.value);
        console.log(phone.value);
        console.log(country.value);
        console.log(terms.checked);

 
        set_cookie("name", name.value, 30);
        set_cookie("email", email.value, 30);
        set_cookie("phone", phone.value, 30);
        set_cookie("country", country.value, 30);
        set_cookie("terms", terms.checked, 30);

        set_cookie("utm_source", utm_source);
        set_cookie("utm_medium", utm_medium);
        set_cookie("utm_campaign", utm_campaign);


        return false;
    });
});

// COOKIE
function set_cookie(name, value, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function get_cookie(name) {
    let nameC = name + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(nameC) == 0) {
            return c.substring(nameC.length, c.length);
        }
    }

    //console.log(name);
    return "";
}

function check_cookie(name) {
    let isCookie = get_cookie(name);
    if (isCookie != "") {
        //alert("Cookie exist " + isCookie);
        console.log("Cookie: " + isCookie);

        send = true;
        cookie = true;
        //submit.classList.add("disabled");
        submit.setAttribute("disabled", "disabled");
        success.classList.remove("d-none");
        success.style.display = "block";

    }

    return cookie;
}
check_cookie("email");
check_cookie("name");
check_cookie("phone");
check_cookie("country");
check_cookie("terms");


if (send) {
    submit.setAttribute("disabled", "disabled");
}
