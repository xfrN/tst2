function add_class_animation(element, animation, delay, prefix = 'wow', duration){
const node = document.querySelector(element);
node.setAttribute('data-wow-delay', `${delay}` || '0.5s');
node.setAttribute('data-wow-duration', `${duration}` || '0.5s');
node.classList.add(`${prefix}`, `${animation}`);
}

add_class_animation('.heading-1', 'fadeInDown');
add_class_animation('.heading-sub', 'fadeInUp', '0.4s');
add_class_animation('.btn-group', 'fadeIn', '0.6s');
add_class_animation('.heading-group-wrap', 'fadeIn', '0.7s');
add_class_animation('.text-block--1', 'fadeIn', '0.8s');
add_class_animation('.row-3', 'fadeInUp', '0.2s');
add_class_animation('.services tr', 'fadeIn', '0.2s');
add_class_animation('.services tr + tr', 'fadeIn', '0.3s');
add_class_animation('.services tr + tr + tr', 'fadeIn', '0.4s');
add_class_animation('.services tr + tr + tr + tr', 'fadeIn', '0.5s');
add_class_animation('.services tr + tr + tr + tr + tr', 'fadeIn', '0.6s');
add_class_animation('.list tr', 'fadeIn', '0.1s');
add_class_animation('.list tr + tr', 'fadeIn', '0.2s');

new WOW().init();
