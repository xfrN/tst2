let cookie = false;

// Cookie
function set_Cookie(name, value, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function get_Cookie(name) {
    let new_name = name + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(new_name) == 0) {
            return c.substring(new_name.length, c.length);
        }
    }

    //console.log(name);
    return "";
}

function check_Cookie(params) {
    let isCookie = get_Cookie(params);
    if (isCookie != "") {
        //alert("Cookie exist " + isCookie);
        console.log("Cookie: " + isCookie);

        send = true;
        cookie = true;
        //submit.classList.add("disabled");
        submit.setAttribute("disabled", "disabled");
        success.style.display = "block";
    }

    return cookie;
}

check_Cookie("cookie_email");
check_Cookie("cookie_name");
check_Cookie("cookie_phone");
check_Cookie("cookie_country");
check_Cookie("cookie_terms");

check_Cookie("cookie_utm_terms");
check_Cookie("cookie_utm_source");
check_Cookie("cookie_utm_campaign");
check_Cookie("cookie_utm_medium");
check_Cookie("cookie_utm_content");
check_Cookie("cookie_utm_ref");