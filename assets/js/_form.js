// import Cookie from '_cookie'
// export {
//    Cookie
// }

let cookie = false;

// Cookie
function set_Cookie(name, value, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function get_Cookie(name) {
    let new_name = name + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(new_name) == 0) {
            return c.substring(new_name.length, c.length);
        }
    }

    //console.log(name);
    return "";
}

function check_Cookie(params) {
    let isCookie = get_Cookie(params);
    if (isCookie != "") {
        //alert("Cookie exist " + isCookie);
        console.log("Cookie: " + isCookie);

        send = true;
        cookie = true;
        //submit.classList.add("disabled");
        submit.setAttribute("disabled", "disabled");
        success.style.display = "block";
    }

    return cookie;
}

check_Cookie("cookie_email");
check_Cookie("cookie_name");
check_Cookie("cookie_phone");
check_Cookie("cookie_country");
check_Cookie("cookie_terms");

check_Cookie("cookie_utm_terms");
check_Cookie("cookie_utm_source");
check_Cookie("cookie_utm_campaign");
check_Cookie("cookie_utm_medium");
check_Cookie("cookie_utm_content");
check_Cookie("cookie_utm_ref");


// VARS
const form = document.querySelector(".form");
const name = document.querySelector("[name=name]");
const email = document.querySelector("[name=email]");
const tel = document.querySelector('[name="phone"]');
const company = document.querySelector('[name="country"]');
const terms = document.querySelector('[name="terms"]');

const submit = document.querySelector(".form [type=submit]");
const success = document.querySelector(".form__msg--success");

const form_source = document.querySelector('[name="user_source"]');
const form_medium = document.querySelector('[name="user_medium"]');
const form_campaign = document.querySelector('[name="user_campaign"]');
const form_content = document.querySelector('[name="user_content"]');
const form_trem = document.querySelector('[name="user_term"]');
const form_ref = document.querySelector('[name="user_ref"]');

let disable = false;
let send = false;

if (send) {
    submit.setAttribute("disabled", "disabled");
}

// FORM send
function checkSendModal(send) {
    if (send) {
        window.localStorage.setItem('send', true);
    }
    return send;
}

function form_values(e){
    e.preventDefault();

    const value = {
        name: name.value,
        email: email.value,
        phone: phone.value,
        country: country.value,
        terms: terms.checked
    }

    alert(values);
    
    set_Cookie(name);
    set_Cookie(email);
    set_Cookie(phone);
    set_Cookie(country);
    set_Cookie(terms);

};

$(function () {
$('.form').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
})
.on('form:submit', function() {

form_values();

    return false; 
});
});